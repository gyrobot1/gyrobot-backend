"use strict";
const crypt = require("bcryptjs");

class User{
	constructor(id, pw){
		this.m_id = id;
		this.m_pwhash = crypt.hash(pw, 12); 
	}
	get id() { return this.m_id; }
	authenticate(pw){
		return crypt.compare(pw, this.m_pwhash);
	}
}

class Authenticator{
	constructor(config){
		this.m_config = config;
		this.m_tokens = config.debug ? ["abcd"] : [];
		this.m_users = {};
		this.m_admin = new User(config.admin.id, config.admin.pw);
		this.m_users[this.m_admin.id] = this.m_admin;
	}
	checkToken(req){
		let token = req.cookies;
		console.log(typeof token);
		 console.log('Cookies: ', req.cookies);

  	// Cookies that have been signed
  	console.log('Signed Cookies: ', req.signedCookies);
		let index = this.m_tokens.indexOf(token);
		return index !== -1;
	}
	authenticate(id, pw){
		if (id in this.m_users){
			return this.m_users[id].authenticate(pw);
		} else return false;
	} 
	get admin() { return this.m_admin; }
}
 
module.exports = Authenticator;

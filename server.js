#!/usr/bin/env node
"use strict"; 
const fs = require('fs');
const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const WebSocketServer = require("./web-socket-server.js");

let app = express();

let config = {
  port: 8082,
  endpoint: "localhost",
  debug: true,
  admin: { 
  	id: "admin",
  	pw: "pw12345"
  }
}

for (let i in process.argv){
  var arg = process.argv[i];
  if (arg.includes("debug")) config.debug = true;
}

let server = app.listen(config.port, function () {
  console.log('Generic websocket server listening on port ' + config.port + '!')
});

app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('./public'));


let wss = new WebSocketServer(server, config);


 

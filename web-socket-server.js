"use strict";
const ws = require('ws');
const { v4: uuid } = require('uuid');
const Authenticator = require("./authenticator.js");

let MessageType = {
	ClientAdded: 1,
	ClientRemoved: 2,
	Init: 3,
	SendAll: 4,
	SendTo: 5,
	Error: 6,
	ChangeRoom: 7,
	Kick: 8,
	CloseRoom: 9
}

class WebSocketHandler{
	constructor(gwss, ws, room){
		this.m_gwss = gwss;
		this.m_ws = ws;
		this.m_uuid = uuid();
		this.m_room = room;
		ws.on("message", (msg) => this.onMessage(msg));
		ws.on("close", (e) => this.onClose(e));
		room.addHandler(this);
	}
	async onMessage(msg){
		if (typeof msg === "object"){
		 	msg = msg.toString();
		} 
		this.handleMessage(msg);
	}
	handleMessage(msg){
		try{
			let json = JSON.parse(msg);
			json.sender = this.uuid;
			switch (json.t){
				case MessageType.SendTo:
					this.m_room.sendTo(json.uuid, JSON.stringify(json));
					break;
				case MessageType.SendAll:
					this.m_room.sendAll(this.uuid, JSON.stringify(json));
					break;
				case MessageType.ChangeRoom:
					this.m_gwss.handleChangeRoom(this, json);
					break;
				default:
					//console.log("default");
					this.sendError(msg);
					break;
			}
		} catch (e){
			console.log(e);
			this.sendError(msg);
		}
	}
	onClose(e){
		//console.log(e);
		this.m_room.onHandlerClosed(this);
		this.m_gwss.onHandlerClosed(this);
	}
	send(msg){
		this.m_ws.send(msg);
	}
	sendError( msg){
		let errorMsg = {
			t: MessageType.Error,
			msg: msg
		}
		this.send(JSON.stringify(errorMsg));
	}
	get uuid(){
		return this.m_uuid;
	}  
	get room() { return this.m_room; }
	set room(value) { 
		if (!value)	throw new Error("room is null");
		this.m_room.removeHandler(this);
		this.m_room = value; 
		this.m_room.addHandler(this);
	}
}

class WebSocketRoom{
	constructor(id, host){
		this.m_id = id;
		this.m_handlers = {}
		this.m_host;
	}
	get id() { return this.m_id; }
	sendAll(sender, msg){
		for (let i in this.m_handlers){
			let h = this.m_handlers[i];
			if (h.uuid != sender) h.send(msg);
		}
	}
	sendTo(uuid, msg){
		let handler = this.m_handlers[uuid].send(msg); 
	}
	sendInit(handler){
		let initMsg = {
			t: MessageType.Init,
			uuid: handler.uuid,
			roomId: this.m_id,
			clients: []
		}
		for (let i in this.m_handlers){
			let h = this.m_handlers[i];
			initMsg.clients.push(h.uuid);
		} 
		handler.send(JSON.stringify(initMsg));
	}
	addHandler(handler){
		this.m_handlers[handler.uuid] = handler;
		this.sendInit(handler);
    this.sendAll(handler.uuid, JSON.stringify({t:MessageType.ClientAdded, uuid: handler.uuid}));
		//console.log("room " + this.m_id + " has: ");
		//console.log(Object.keys(this.m_handlers));
	}
	removeHandler(handler){
		delete this.m_handlers[handler.uuid];
		let msg = {
			t: MessageType.ClientRemoved,
			uuid: handler.uuid
		}
		this.sendAll(handler.uuid, JSON.stringify(msg));
		if (Object.keys(this.m_handlers).length == 0) {
			this.roomEmpty(this);
		}
	}
	roomEmpty(room){
		
	}
	onHandlerClosed(handler){
		this.removeHandler(handler);
	}
}

class WebSocketServer{
	constructor(server, config){
		this.config = config;
		this.server = server;
		this.rooms = { default: new WebSocketRoom("default") }
		this.handlers = {}
		this.m_auth = new Authenticator(config);
		this.wss = new ws.Server({
      server: server,
      path: "/ws"
    });
    this.wss.on("connection", (webSocket) => this.onConnection(webSocket)); 
    this.server.on("upgrade", (request, socket)=> this.onUpgrade(request, socket));
    if (this.config.debug) console.log("wss created");
	}
	onHandlerClosed(handler){
		delete this.handlers[handler.uuid];
	}
	
	onConnection(ws){
		let handler = new WebSocketHandler(this, ws, this.rooms.default);
    this.handlers[handler.uuid] = handler;
	}
	onUpgrade(req, socket, head){
		if (this.m_auth.checkToken(req)) { 
			this.wss.handleUpgrade(req, socket, head, function done(ws) {
		    this.wss.emit('connection', ws, request, client);
		  });
		} else {
			socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
      socket.destroy();
		}
	}
	handleChangeRoom(handler, msg){
		let id = msg.id;
		let room;
		if (id in this.rooms){
			room = this.rooms[id];
		} else {
			room = new WebSocketRoom(id, handler.uuid);
			this.rooms[id] = room;
			room.roomEmpty = (room) => this.onRoomEmpty(room);
		}
		handler.room = room;
	} 
	onRoomEmpty(room){
		delete this.rooms[room.id];
	} 
}
 
module.exports = WebSocketServer;


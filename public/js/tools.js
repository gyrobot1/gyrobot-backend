"use strict";

export {
	tools
}

class Tools{
	constructor(){
	}
	async loadView(name){
		let path = "/views/" + name + ".html";
		let options = {
			method: "GET",
			headers: {
				"Content-Type": "html/text"
			}
		}
		let response = await fetch(path, options); 
		let content = document.getElementById("content");
		content.innerHTML = await response.text();
	}
}

const tools = new Tools 

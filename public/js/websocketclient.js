"use strict";

export {
	GyrobotWebSocketClient
}

let MessageType = {
	ClientAdded: 1,
	ClientRemoved: 2,
	Init: 3,
	SendAll: 4,
	SendTo: 5,
	Error: 6,
	ChangeRoom: 7
}

class GyrobotWebSocketClient{
	constructor(config){
		this.config = config;
		this.ws = null;
		this.uuid = "";
		this.clients = [];
		this.openWebSocket();
   	console.log("wsc created");
	}
	openWebSocket(){
		this.ws = new WebSocket(this.config.endpoint);
		this.uuid = "";
		this.clients = [];
		this.ws.onopen = (e) => this.onOpen(); 
    this.ws.onclose = (e) => this.onClose(e);
    this.ws.onmessage = (e) => this.onMessage(e);
	}
	onOpen(){
		console.log("ws client opened");
		this.opened();
	}
	onClose(e){
		console.log("ws closed reopening");
		//console.log(e);
		this.ws.close();
		this.ws = null;
		this.openWebSocket();
	}
	opened(){}
	async onMessage(e) {
		let msg = "";
		if (typeof e.data === "object") {
			msg = await e.data.text();
		}	else msg = e.data;
		let json = JSON.parse(msg);
		this.handleClientMessage(json);
	}

	handleClientMessage(msg){
		switch (msg.t){
			case MessageType.Init:
				this.handleInit(msg);
				break;
			case MessageType.ClientAdded:
				if (msg.uuid != this.uuid) this.clients.push(msg.uuid);
				break;
			case MessageType.ClientRemoved:
				let i = this.clients.indexOf(msg.uuid);
				this.clients.splice(i,1);
				break;
			case MessageType.SendAll:
			case MessageType.SendTo:
				if (msg.sender != this.uuid){
					this.clientMessage(msg.sender, msg.d);				
				}
				break; 
			default:
				console.log(msg);
		}
	}
	handleInit(msg){
		this.clients = [];
		this.uuid = msg.uuid;
		this.room = msg.roomId;
		for (let i in msg.clients){
			let client = msg.clients[i];
			if (this.uuid != client) this.clients.push(client);
		}
	} 
	clientMessage(sender, msg) {
		if (sender != this.uuid){
		}
		console.log("message from: " + sender );
		console.log(msg);
	}
	sendAll(data){
		let msg = {
			t: MessageType.SendAll,
			d: data
		}
		this.ws.send(JSON.stringify(msg));
	}
	sendTo(uuid, data){
		let msg = {
			t: MessageType.SendAll,
			uuid: uuid, 
			d: data
		}
		this.ws.send(JSON.stringify(msg));
	}
	changeRoom(id){
		let msg = {
			t: MessageType.ChangeRoom,
			id: id
		}
		this.ws.send(JSON.stringify(msg));
	}
}

"use strict";
import { GyrobotWebSocketClient } from "./websocketclient.js"
import { GyrobotHandler } from "./gyrobothandler.js"

let wsp = location.protocol === "https:" ? "wss://" : "ws://";
let config = {
	endpoint: wsp + "gyrobot.pracedru.dk/ws"
}

let wsc = null;
let hnd = null;

function init(){
	wsc = new GyrobotWebSocketClient(config);
	hnd = new GyrobotHandler(config, wsc);
	window.wsc = wsc;
}

window.init = init;



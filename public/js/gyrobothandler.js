"use strict";
import { tools } from "./tools.js"
export {
	GyrobotHandler
}

let BalanceComponentType = {
	Proportional: 0,
	Integral: 1,
	Differential: 2,
	Overshoot: 3
};

let GyrobotMessageType = {
	ComponentsData: 0,
	Start: 1,
	Stop: 2,
	Init: 3,
	Orientation: 4,
	DirInput: 5
}

let GyrobotSenderType = {
	Robot: 0,
	Controller: 1
}

let ViewType = {
	None: 0,
	Home: 1,
	Settings: 2,
	WSSettings: 3
}

class GyrobotHandler{
	constructor(config, wsc){
		this.config = config;
		this.wsc = wsc;
		this.sldCmps = null
		this.cmpData = {};
		this.started = false;
		this.sldBalPnt = null;
		this.compassimage = new Image();
		this.compassimage.src = "/img/compass.webp";
		this.o = { x: 0, y: 1, z: 0}
		this.input = { f: false, b: false, l: false, r: false }
		this.view = ViewType.None;
		this.homeButton = document.getElementById("homeButton");
		this.settingsButton = document.getElementById("settingsButton");
		this.wsSettingsButton = document.getElementById("wsSettingsButton");
		this.gyroCanvas = null; // document.getElementById("gyroCanvas");
		this.counter = 0;
		homeButton.onmousedown = () => this.onHome(this);
		settingsButton.onmousedown = () => this.onSettings(this);
		wsSettingsButton.onmousedown = () => this.onWSSettings(this);
		wsc.clientMessage = (sender, msg) => this.onClientMessage(sender, msg);
	  wsc.opened = () => this.onWSOpened();
		this.onHome(this);
		this.updateFrame();
	}
	clearView(){
		this.wsSettingsButton.classList.remove("Selected");
		this.homeButton.classList.remove("Selected");
		this.settingsButton.classList.remove("Selected");
		document.getElementById("content").innerHTML = "";
		this.sldCmps = null;
		this.gyroCanvas = null;
	}
	onWSOpened(){
		this.wsc.changeRoom(localStorage.getItem("room"));
	}
	drawCanvas(){
		let ctx = this.gyroCanvas.getContext("2d");
		let mid = { x: this.gyroCanvas.width/2, y: this.gyroCanvas.height/2 }
		let a = Math.atan2(this.o.z, this.o.y);
		
		ctx.clearRect(0, 0, this.gyroCanvas.width, this.gyroCanvas.height);
		ctx.save();
		ctx.translate(mid.x, mid.y);
		ctx.rotate(a);
		ctx.drawImage(this.compassimage,-125,-125, 250,250);
		ctx.restore();
	}
	updateFrame () {
		if (this.gyroCanvas){
			try{
				this.drawCanvas();
			} catch (e) {
				console.log(e);
			}		
		}
		//window.requestAnimationFrame(()=>this.updateFrame());
	}
	async onWSSettings(self){
		if (this.view == ViewType.WSSettings) return;
		self.clearView();
		this.wsSettingsButton.classList.add("Selected");
		await tools.loadView("wssettings");
		self.view = ViewType.WSSettings;
		let roomInput = document.getElementById("roomInput");
		roomInput.onchange = () => self.onRoomInputChanged();
		roomInput.value = localStorage.getItem("room");
	}
	async onHome(self){
		if (this.view == ViewType.Home) return;
		self.clearView();
		this.homeButton.classList.add("Selected");
		await tools.loadView("home");
		self.view = ViewType.Home;
		self.stopButton = document.getElementById("stopButton");
		self.startButton = document.getElementById("startButton");
		let fButton = document.getElementById("fButton");
		let bButton = document.getElementById("bButton");
		let lButton = document.getElementById("lButton");
		let rButton = document.getElementById("rButton");
		self.startButton.onclick = () => self.onStart();
		self.stopButton.onclick = () => self.onStop();
		self.setTouchButtenEvents(fButton, self.onForward);
		self.setTouchButtenEvents(bButton, self.onBackward);
		self.setTouchButtenEvents(lButton, self.onLeft);
		self.setTouchButtenEvents(rButton, self.onRight);
		this.gyroCanvas = document.getElementById("gyroCanvas");
		this.gyroCanvas.height = 300;
		self.updateHomeView();
	}
	setTouchButtenEvents(button, onPressFunction){
		button.oncontextmenu = () => { return false; }
		button.ontouchstart = () => onPressFunction(this, true);
		button.ontouchend = () => onPressFunction(this, false);
	}
	
	async onSettings(self){
		if (this.view == ViewType.Settings) return;
		self.clearView();
		this.settingsButton.classList.add("Selected");
		await tools.loadView("balancesettings");
		self.view = ViewType.Settings;
		let sldProp = { f: null, l: null, d: null }
		let sldInt = { f: null, l: null, d: null }
		let sldDiff = { f: null, l: null, d: null }
		let sldOver = { f: null, l: null, d: null }
		self.sldCmps = {}
		self.sldCmps[BalanceComponentType.Proportional] = sldProp;
		self.sldCmps[BalanceComponentType.Integral] = sldInt;
		self.sldCmps[BalanceComponentType.Differential] = sldDiff;
		self.sldCmps[BalanceComponentType.Overshoot] = sldOver;
		self.sldBalPnt = new GyrobotSlider("balPnt", "Balance point", (v) => this.onBalChange(v), -1, 1);
		sldProp.f = new GyrobotSlider("propFactor", "Proportional factor", (v) => this.onChange(BalanceComponentType.Proportional, "f", v), 0.2, 5.0);
		sldProp.l = new GyrobotSlider("propLerp", "Proportional lerp", (v) => this.onChange(BalanceComponentType.Proportional, "l", v));
		sldProp.d = new GyrobotSlider("propDamp", "Proportional damping", (v) => this.onChange(BalanceComponentType.Proportional, "d", v));
		sldInt.f = new GyrobotSlider("intFactor", "Integral factor", (v) => this.onChange(BalanceComponentType.Integral, "f", v), 0.0, 1.0);
		sldInt.l = new GyrobotSlider("intLerp", "Integral lerp", (v) => this.onChange(BalanceComponentType.Integral, "l", v));
		sldInt.d = new GyrobotSlider("intDamp", "Integral damping", (v) => this.onChange(BalanceComponentType.Integral, "d", v));		
		sldDiff.f = new GyrobotSlider("diffFactor", "Differential factor", (v) => this.onChange(BalanceComponentType.Differential, "f", v), 0.0, 0.5);
		sldDiff.l = new GyrobotSlider("diffLerp", "Differential lerp", (v) => this.onChange(BalanceComponentType.Differential, "l", v));
		sldDiff.d = new GyrobotSlider("diffDamp", "Differential damping", (v) => this.onChange(BalanceComponentType.Differential, "d", v));
		sldOver.f = new GyrobotSlider("overFactor", "Overshoot factor", (v) => this.onChange(BalanceComponentType.Overshoot, "f", v), 0.0, 1.0);
		sldOver.l = new GyrobotSlider("overLerp", "Overshoot lerp", (v) => this.onChange(BalanceComponentType.Overshoot, "l", v));
		sldOver.d = new GyrobotSlider("overDamp", "Overshoot damping", (v) => this.onChange(BalanceComponentType.Overshoot, "d", v));	
		self.stopButton = document.getElementById("stopButton");
		self.startButton = document.getElementById("startButton");
		self.loadButton = document.getElementById("loadButton");
		self.saveButton = document.getElementById("saveButton");
		startButton.onmousedown = () => self.onStart();
		stopButton.onmousedown = () => self.onStop();
		loadButton.onmousedown = () => self.onLoad();
		saveButton.onmousedown = () => self.onSave();
		self.updateSettingsView();
	}
	onRoomInputChanged(){
		let roomInput = document.getElementById("roomInput");
		let room = roomInput.value;
		localStorage.setItem("room", room);
		this.wsc.changeRoom(room);
	}
	handleComponentData(msg){
		this.cmpData = msg;
		this.updateView();
	}
	updateView(){
		switch (this.view) {
			case ViewType.Settings:
				this.updateSettingsView();
				break;
			case ViewType.Home:
				this.updateHomeView();
				break;
			default:
				break;
		}
	}
	updateSettingsView(){
		let msg = this.cmpData;
		if (this.sldBalPnt) this.sldBalPnt.setActual(msg.bo);
		for (let i in msg.cmps){
			let cmp = msg.cmps[i];
			let compsliders = this.sldCmps[cmp.bct];
			if (compsliders){
				compsliders.f.setActual(cmp.f);
				compsliders.l.setActual(cmp.l);
				compsliders.d.setActual(cmp.d);
			}
		}
		startButton.disabled = this.started;
		stopButton.disabled = !this.started;
	}
	updateHomeView(){
		startButton.disabled = this.started;
		stopButton.disabled = !this.started;
	}
	onClientMessage(sender, msg){
		//if (msg.s != GyrobotSenderType.Robot) return;
		switch (msg.t){
			case GyrobotMessageType.ComponentsData:
				this.handleComponentData(msg);
				break;
			case GyrobotMessageType.Start:
				this.started = true;	
				this.updateView();
				break;
			case GyrobotMessageType.Stop:
				this.started = false;
				this.updateView();
				break;
			case GyrobotMessageType.Orientation:
				this.o = msg;
				this.updateFrame();
				break;
		}
	}
	onForward(self, pressed){
		self.input.f = pressed;
		self.sendInput();
		
	}
	onLeft(self, pressed){
		self.input.l = pressed;
		self.sendInput();
		
	}
	onRight(self, pressed){
		self.input.r = pressed;
		self.sendInput();
	}
	onBackward(self, pressed){
		self.input.b = pressed;
		self.sendInput();
	}
	sendInput(){
		let ivals = this.input.f ? 1 : 0;
		ivals += this.input.l ? 2 : 0;
		ivals += this.input.r ? 4 : 0;
		ivals += this.input.b ? 8 : 0; 
		let msg = {
			i: ivals,
			t: GyrobotMessageType.DirInput, 
			s: GyrobotSenderType.Controller 
		}
		wsc.sendAll(msg);
	}
	
	onStart(){
		let msg = {
			t: GyrobotMessageType.Start, 
			s: GyrobotSenderType.Controller 
		}
		this.wsc.sendAll(msg);
	}
	onStop(){
		let msg = {
			t: GyrobotMessageType.Stop, 
			s: GyrobotSenderType.Controller 
		}
		this.wsc.sendAll(msg);
	}
	onBalChange (v){
		let msg = {
			bo: v,
			t: GyrobotMessageType.ComponentsData, 
			s: GyrobotSenderType.Controller 
		}
		wsc.sendAll(msg);
	}
	onChange(bct, p, v){
		let cmp = { 
			bct: bct
		}
		cmp[p] = v;
		let msg = {
			cmps: [cmp],
			t: GyrobotMessageType.ComponentsData, 
			s: GyrobotSenderType.Controller 
		}
		this.wsc.sendAll(msg);
	}
	onLoad(){
		try {
			let savesData = localStorage.getItem("saves");
			let saves = JSON.parse(savesData);
			this.handleComponentData(saves);
			this.wsc.sendAll(saves);
		} catch (e) {
			console.log(e);
		}
	}
	onSave(){
		let save = {
			cmps: [],
			bo: this.sldBalPnt.value,
			t: GyrobotMessageType.ComponentsData, 
			s: GyrobotSenderType.Controller 
		}
		for (let i in BalanceComponentType){
			let bct = BalanceComponentType[i] ;
			let sldCmp = this.sldCmps[bct];
			let cmp = { 
				bct: bct
			}
			cmp["f"] = sldCmp.f.value;
			cmp["l"] = sldCmp.l.value;
			cmp["d"] = sldCmp.d.value;
			save.cmps.push(cmp);
		}
		let saveData = JSON.stringify(save);
		//console.log(saveData);
		localStorage.setItem("saves", saveData);
	}
}

class GyrobotSlider{
	constructor(parentId, name, changeCallback, min=0.0, max=1.0){
		let content = document.getElementById("balanceSettinsContainter");
		let parent = document.createElement("div");
		parent.classList.add("SlideContainer");
		content.appendChild(parent);
		this.slider = document.createElement("input");
		this.nameLabel = document.createElement("span");
		this.valueLabel = document.createElement("span");
		this.actualLabel = document.createElement("span");
		this.callback = changeCallback;
		
		parent.appendChild(this.nameLabel);
		parent.appendChild(this.valueLabel);
		parent.appendChild(this.actualLabel);
		parent.appendChild(this.slider);
		this.nameLabel.style.display = "inline-block";
		this.nameLabel.style.width = "50%";
		this.nameLabel.innerHTML = name;
		this.valueLabel.innerHTML = "0.000"
		this.actualLabel.style.float = "right";
		this.actualLabel.innerHTML = "0.000"
		
		this.slider.oninput = () => this.onSliderInput();
		this.slider.onchange = () => this.onSliderChange();
		this.slider.type = "range";
		this.slider.classList.add("Slider");
		this.slider.min = 0;
		this.slider.max = 1000;
		this.slider.value = 500;
		this.min = min;
		this.max = max;
		this.setActual(parseFloat(0));
	}
	
	get value(){
		let v = this.min + (this.max-this.min) * this.slider.value/this.slider.max;
		return v;
	}
	
	onSliderInput(){
		this.valueLabel.innerHTML = this.value.toFixed(3);
	}
	onSliderChange(){
		//console.log("onSliderChange");
		this.onSliderInput();
		if (this.callback) this.callback(this.value);
	}
	setActual(v){
		if (v==null) return;
		let t = v.toFixed(3);
		if (this.actualLabel.innerHTML != t) {
			this.actualLabel.innerHTML = t;
			this.slider.value = this.slider.max*((v-this.min)/(this.max-this.min));
			this.valueLabel.innerHTML = t;
		}
	}
}

